#!/bin/zsh

# pick repeated inputs (that don't change with detector number)
droneDir="/Users/bsmiley/work/drone flights/launch point 2/45"
image_root_PAN="Internal_PAN_a01_20210929T115728Z"

# loop over each of the 3 detectors
for d in 1 2 3
do

	# set up inputs that depend on detector number
	image_id_PAN=${image_root_PAN}_detector_${d}
	image_PAN=${image_id_PAN}.tif

	# activate the relevant python virtualenv for NUC and BPI by changing directory to pretty
	cd ../pretty
	
	# remove non-uniformity using the NUC file
	python src/remove_nonuniformity.py \
		--input-dir "${droneDir}" \
		--image ${image_PAN} \
		--nuc-file "${droneDir}/${image_root_PAN}.RAD" \
		--file-type "PAN" \
		--output-dir "${droneDir}/0_nucOut" \
		--use-local-fs \
#		--display-comparison

	# remove bad pixels using the BPM
	python src/interpolate_bad_pixels.py \
		--input-dir "${droneDir}/0_nucOut" \
		--image ${image_PAN} \
		--bpm-file "${droneDir}/${image_root_PAN}.BPM" \
		--file-type "PAN" \
		--output-dir "${droneDir}/1_bpOut" \
		--use-local-fs \
#		--display-comparison 
		
		
	# change to the tiling directory to get the right pyenv settings	
	cd ../tiling
	
	# tile with buffer for the upcoming deconvolve
	python src/tiling.py \
		--input-dir "${droneDir}/1_bpOut" \
		--image ${image_PAN} \
		--file-type "PAN" \
		--output-dir "${droneDir}/2_tilingOut" \
		--use-local-fs		

	# change directory to deconvolve to get the right pyenv settings
	cd ../deconvolve
	
	# send every tile through deconvolve
	for r in 000 001
	do

   		# deconvolve
   		python src/deconvolve.py \
			--input-dir "${droneDir}/2_tilingOut" \
			--output-dir "${droneDir}/3_deconvolveOut" \
			--image "${image_id_PAN}_${r}_000.tif" \
			--file-type "PAN" \
			--use-local-fs \
#			--display-comparison
			
	done # end of tile loop

	# change back to tiling for untile
	cd ../tiling
	
	# untile
	python src/untiling.py \
		--input-dir "${droneDir}/3_deconvolveOut" \
		--input-id ${image_id_PAN} \
		--file-type "PAN" \
		--tile-layout-file "${droneDir}/2_tilingOut/${image_id_PAN}_tile_layout.json" \
		--output-dir "${droneDir}/4_untilingOut" \
		--use-local-fs		

done # end of d loop
