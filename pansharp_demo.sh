#!/bin/bash

# pick repeated inputs
droneDir="/Users/bsmiley/work/drone flights/launch point 2/45"

image_root_PAN="Internal_PAN_a01_20210929T115728Z"
image_id_PAN=${image_root_PAN}_detector_1
image_PAN=${image_id_PAN}.tif

image_root_MS="Internal_MS_a01_20210929T115728Z"
image_id_MS=${image_root_MS}_detector_1
image_MS=${image_id_MS}.tif


# apply NUC
python pretty/src/remove_nonuniformity.py --file-type "PAN" --input-dir "${droneDir}" --image ${image_PAN} --nuc-file "${image_root_PAN}.RAD" --output-dir "${droneDir}/0_nucOut"
python pretty/src/remove_nonuniformity.py --file-type "MS" --input-dir "${droneDir}" --image ${image_MS} --nuc-file "${image_root_MS}.RAD" --output-dir "${droneDir}/0_nucOut"

# tile
python tiling/src/tiling.py --file-type "PAN" --input-dir "${droneDir}/0_nucOut" --image ${image_PAN} --output-dir "${droneDir}/1_tilingOut"
python tiling/src/tiling.py --file-type "MS" --input-dir "${droneDir}/0_nucOut" --image ${image_MS} --output-dir "${droneDir}/1_tilingOut"

# send every tile through
for r in 000 001
do

   # deconvolve
   python deconvolve/src/deconvolve.py --local --file-type "PAN" --input-dir "${droneDir}/1_tilingOut" --image-id ${image_id_PAN} --tile-id ${r}_000 --output-dir "${droneDir}/2_deconvolveOut"
   python deconvolve/src/deconvolve.py --local --file-type "MS" --input-dir "${droneDir}/1_tilingOut" --image-id ${image_id_MS} --tile-id ${r}_000 --output-dir "${droneDir}/2_deconvolveOut"

   # pansharp
   python pansharp/src/make_pansharp.py --input-dir "${droneDir}/2_deconvolveOut" --pan-image-id ${image_id_PAN} --ms-image-id ${image_id_MS} --tile-id ${r}_000 --output-dir "${droneDir}/3_pansharpOut"

done

# untile
image_root_PANSHARP=`echo ${image_id_PAN} | sed s/PAN/PANSHARP/`
python tiling/src/untiling.py --input-dir "${droneDir}/3_pansharpOut" --input-id ${image_root_PANSHARP} --file-type "PANSHARP" --output-dir "${droneDir}/4_untileOut"

